import React from "react";
import ShallowRenderer from "react-test-renderer/shallow";
import App from "./App";

describe("App", () => {
  it("should match snapshot", () => {
    const renderer = new ShallowRenderer();
    const tree = renderer.render(<App />);
    expect(tree).toMatchSnapshot();
  });
});
