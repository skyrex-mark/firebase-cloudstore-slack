import { storiesOf } from "@storybook/react";
import React from "react";
import Sidebar from ".";
import { data } from "./__mocks__";

storiesOf("Sidebar", module).add("with text", () => <Sidebar {...data} />);
