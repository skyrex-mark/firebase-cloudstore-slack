import React from "react";
import ShallowRenderer from "react-test-renderer/shallow";
import Sidebar from ".";
import { data } from "./__mocks__";

describe("App", () => {
  it("should match snapshot", () => {
    const renderer = new ShallowRenderer();
    const tree = renderer.render(<Sidebar {...data} />);
    expect(tree).toMatchSnapshot();
  });
});
