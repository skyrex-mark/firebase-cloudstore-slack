import React, { Fragment } from "react";
import { Header, List } from "semantic-ui-react";

const Sidebar = ({ name, channelList }) => (
  <Fragment>
    <Header size="large">{name}</Header>
    <List divided relaxed>
      {channelList.map((channel, index) => (
        <List.Item key={index}>
          <List.Icon name="github" size="large" verticalAlign="middle" />
          <List.Content>
            <List.Header># {channel.name}</List.Header>
          </List.Content>
        </List.Item>
      ))}
    </List>
  </Fragment>
);

export default Sidebar;
