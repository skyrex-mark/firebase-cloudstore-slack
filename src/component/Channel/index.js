import React from 'react';
import { Grid, List, Input, Label } from 'semantic-ui-react';

class Channel extends React.Component {
  state = {
    channelName: ''
  };

  handleChange = e => {
    this.setState({ channelName: e.target.value });
  };

  handleEnter = e => {
    if (e.key === 'Enter') {
      this.props.createNewChannel(e.target.value);
      this.setState({ channelName: '' });
    }
  };

  render() {
    return (
      <Grid.Column mobile={6} tablet={4} computer={3} stretched>
        <Grid.Row>
          <List>
            {this.props.channels.map((channel, index) => (
              <List.Item key={index} as="a">
                {`# ${channel}`}
              </List.Item>
            ))}
          </List>
        </Grid.Row>
        <Grid.Row style={{ maxHeight: '3rem' }}>
          <Input
            style={{ width: '100%', padding: 0 }}
            labelPosition="right"
            type="text"
            placeholder="Channel Name"
            onKeyPress={this.handleEnter}
            value={this.state.channelName}
            onChange={this.handleChange}
          >
            <Label basic>#</Label>
            <input />
          </Input>
        </Grid.Row>
      </Grid.Column>
    );
  }
}
export default Channel;
