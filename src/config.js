import firebase from "firebase";

// Initialize Firebase
const config = {
  apiKey: "AIzaSyDCszTxBETy_NoJhci5j64tAmTD9_YzXmg",
  authDomain: "aye-xshmuj.firebaseapp.com",
  databaseURL: "https://aye-xshmuj.firebaseio.com",
  projectId: "aye-xshmuj",
  storageBucket: "aye-xshmuj.appspot.com",
  messagingSenderId: "445949974871"
};

const defaultApp = firebase.initializeApp(config);
const firestore = defaultApp.firestore();

// to remove firestore warning
const settings = { timestampsInSnapshots: true };
firestore.settings(settings);

export { firestore };
export default defaultApp;
