import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react';
import './App.css';

import Channel from './component/Channel';
import { firestore } from './config';

const styles = {
  grid: { margin: 0, height: '100vh' },
  gridColumn: { padding: 0 },
  header: {
    maxHeight: '3rem'
  },
  input: { maxHeight: '4rem' }
};

class App extends Component {
  state = {
    channels: []
  };

  fetchChannels = async () => {
    const document = firestore
      .collection('chat app')
      .doc('XoDkhnysK4zL0Vewsz7r');
    const doc = await document.get();
    this.setState({
      channels: doc.data().channels
    });
  };

  createNewChannel = name => {
    firestore
      .collection('chat app')
      .doc('XoDkhnysK4zL0Vewsz7r')
      .set({
        channels: [...this.state.channels, name]
      });
    this.fetchChannels();
  };

  componentDidMount = async () => {
    this.fetchChannels();
  };

  render() {
    return (
      <Grid style={styles.grid}>
        <Channel
          channels={this.state.channels}
          createNewChannel={this.createNewChannel}
        />
        <Grid.Column
          mobile={10}
          tablet={12}
          computer={13}
          style={styles.gridColumn}
          stretched
        >
          <Grid.Row style={styles.header}>Header</Grid.Row>
          <Grid.Row>Chat logs</Grid.Row>
          <Grid.Row style={styles.input}>Messages to send</Grid.Row>
        </Grid.Column>
      </Grid>
    );
  }
}

export default App;
